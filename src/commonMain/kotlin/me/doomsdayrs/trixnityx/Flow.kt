package me.doomsdayrs.trixnityx

import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.channelFlow
import kotlinx.coroutines.launch

/**
 * 03 / 02 / 2023
 *
 * Take items from the flow for a set amount of time.
 *
 * @param timeoutMillis time in milliseconds until flow should be closed.
 */
public fun <T> Flow<T>.takeUntilTimeout(timeoutMillis: Long): Flow<T> = channelFlow {
	val collector = launch {
		collect {
			send(it)
		}
		close()
	}
	delay(timeoutMillis)
	collector.cancel()
	close()
}