package me.doomsdayrs.trixnityx

import kotlinx.coroutines.flow.firstOrNull
import net.folivo.trixnity.client.room.RoomService
import net.folivo.trixnity.client.room.message.MessageBuilder
import net.folivo.trixnity.client.store.TimelineEvent
import net.folivo.trixnity.core.model.RoomId

/*
 * Copyright (C) 2023 Doomsdayrs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * Works on [RoomService.sendMessage].
 *
 * Sends a message, then gets the event for said message.
 *
 * @param roomId room to listen to.
 * @param keepMediaInCache [RoomService.sendMessage].
 * @param timeout delay in milliseconds till giving up and returning a null.
 *
 * @return TimelineEvent of the message,
 *  or null if something went wrong or timed out.
 */
public suspend fun RoomService.dispatchMessage(
	roomId: RoomId,
	keepMediaInCache: Boolean = true,
	timeout: Long = 5000,
	builder: suspend MessageBuilder.() -> Unit
): TimelineEvent? {
	val transactionId = sendMessage(roomId, keepMediaInCache, builder)
	val message = getTimelineEventsFromNowOn()
		.takeUntilTimeout(timeout)
		.firstOrNull {
			it.event.unsigned?.transactionId == transactionId
		}
	return message
}