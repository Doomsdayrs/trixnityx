/*
 * Copyright (C) 2022 Doomsdayrs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package me.doomsdayrs.trixnityx

import io.ktor.http.ContentType
import kotlinx.coroutines.flow.MutableStateFlow
import net.folivo.trixnity.client.MatrixClient
import net.folivo.trixnity.client.media
import net.folivo.trixnity.clientserverapi.model.media.FileTransferProgress
import net.folivo.trixnity.core.toByteArrayFlow

/*
 * 02 / 06 / 2022
 */

/**
 * Upload media and get the MXC URI
 *
 * @param bytes ByteArray of data to upload
 * @param type Content type of the data
 * @param progress Progress of the upload
 * @return MXC URI https://matrix.org/docs/spec/client_server/r0.6.0#mxc-uri
 */
public suspend fun MatrixClient.uploadMedia(
	bytes: ByteArray,
	type: ContentType,
	progress: MutableStateFlow<FileTransferProgress?>? = null
): Result<String> {
	val cachedUri = media.prepareUploadMedia(bytes.toByteArrayFlow(), type)
	return media.uploadMedia(cachedUri, progress)
}