/*
 * Copyright (C) 2022 Doomsdayrs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package me.doomsdayrs.trixnityx

import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import net.folivo.trixnity.client.MatrixClient
import net.folivo.trixnity.client.room
import net.folivo.trixnity.client.room.flatten
import net.folivo.trixnity.client.store.Room
import net.folivo.trixnity.core.model.RoomId
import net.folivo.trixnity.core.model.events.m.room.Membership

/*
 * 02 / 06 / 2022
 */

/**
 * Get all rooms that you are invited to
 */
public fun MatrixClient.getRoomsInvitedTo(): Flow<List<Room>> =
	room.getAll().flatten().map { rooms ->
		rooms.filter { it.membership == Membership.INVITE }
	}


/**
 * Blocking function, Will stop any and all further code.
 * @param interject Decide if you want to join the room or not
 * @param callback See the result of joining the room
 */
public suspend fun MatrixClient.joinAllInvites(
	interject: (Room) -> Boolean = { true },
	callback: (Result<RoomId>) -> Unit = {}
) {
	getRoomsInvitedTo().collect { rooms ->
		rooms.forEach { room ->
			if (interject(room))
				api.rooms.joinRoom(room.roomId).also(callback)
		}
	}
}