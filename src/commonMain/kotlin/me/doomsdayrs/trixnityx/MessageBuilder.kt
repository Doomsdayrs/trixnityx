/*
 * Copyright (C) 2022 Doomsdayrs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package me.doomsdayrs.trixnityx

import net.folivo.trixnity.client.room.message.MessageBuilder
import net.folivo.trixnity.core.model.EventId
import net.folivo.trixnity.core.model.events.RelatesTo

/*
 * 28 / 06 / 2022
 */

/**
 * Mark a message as a reply
 *
 * @param eventId event to reply to
 */
@Suppress("DeprecatedCallableAddReplaceWith")
@Deprecated("Replace with reply()")
public fun MessageBuilder.replyTo(eventId: EventId) {
	relatesTo = RelatesTo.Reply(RelatesTo.ReplyTo(eventId))
}